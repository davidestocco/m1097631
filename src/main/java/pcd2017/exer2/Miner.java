package pcd2017.exer2;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Suggested element for the Stream-based algorithm.
 */
class Candidate {
  final int testNonce;
  final String hash;

  Candidate(int testNonce, String hash) {
    this.testNonce = testNonce;
    this.hash = hash;
  }
}

public class Miner {

  /**
   * Parallel-stream-based mining algorithm.
   * 
   * @param data
   * @return New Block with Nonce, Hash and Data
   */
  public Block streamMine(BlockData data) {
    Candidate c = Stream
                .iterate(Integer.MIN_VALUE,i->i+1)
                .parallel()
                .map(i-> new Candidate(i, sha256(Miner.canonical(data,i))))
                .filter(i->isTarget(i))
                .findFirst()
                .get();
    return  new Block(c.testNonce, c.hash, data);
  }

  /**
   * Thread-based mining algorithm.
   * 
   * @param data
   * @return New Block with Nonce, Hash and Data
   * @throws InterruptedException
   * @throws ExecutionException
   */
  public Block threadMine(BlockData data) throws ExecutionException, InterruptedException {
        int cores = Runtime.getRuntime().availableProcessors();
        AtomicInteger i = new AtomicInteger(0);
        ExecutorService eS = Executors.newFixedThreadPool(cores);
        ArrayList<Callable<Candidate>> tasks = new ArrayList<Callable<Candidate>>();
        for (int j = 0; j < cores; j++)
        tasks.add(() -> {
              Candidate tryCandidate;
              do {
                    i.getAndIncrement();
                    tryCandidate = new Candidate(i.get(), sha256(canonical(data, i.get())));
                } while (!isTarget(tryCandidate));
              return tryCandidate;
          });
          Candidate c = eS.invokeAny(tasks);
          eS.shutdown();
          return new Block(c.testNonce, c.hash, data);
  }

  /**
   * Create the canonical representation of the blockdata with the nonce, to be fed to the hash function.
   * 
   * DO NOT MODIFY
   * 
   * @param input data to hash
   * @param nonce nonce to hash with the data
   * @return
   */
  public static String canonical(BlockData input, int nonce) {
    return nonce + ":" + input.previousHash + ":" + input.timestamp + ":"
        + input.data;
  }

  /**
   * Calculate the hash of the input
   * 
   * DO NOT MODIFY
   * 
   * @param input data to hash
   * @return hash value
   */
  public static String sha256(String input) {
    try {
      MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
      byte[] result = sha256.digest(input.getBytes());
      return bytesToHex(result);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException("This should not have happened");
    }
  }

  /**
   * Pretty-prints a byte array.
   * 
   * DO NOT MODIFY
   * 
   * @param hash
   * @return
   */
  private static String bytesToHex(byte[] hash) {
    StringBuilder hexString = new StringBuilder();
    for (int i = 0; i < hash.length; i++) {
      String hex = Integer.toHexString(0xff & hash[i]);
      if (hex.length() == 1)
        hexString.append('0');
      hexString.append(hex);
    }
    return hexString.toString();
  }

  public static final String HASH_PREFIX = "42424";

  /**
   * Checks if an hash is the target value
   * 
   * DO NOT MODIFY
   * 
   * @param hash the hash to check
   * @return true if it satisfies the target condition
   */
  public static boolean isTarget(Candidate candidate) {
    return candidate.hash.startsWith(HASH_PREFIX);
  }

  /**
   * Checks if an hash is the target value
   * 
   * DO NOT MODIFY
   * 
   * @param hash the hash to check
   * @return true if it satisfies the target condition
   */
  public static boolean isTarget(String hash) {
    return hash.startsWith(HASH_PREFIX);
  }

  /**
   * Checks if the block is valid
   * 
   * DO NOT MODIFY
   * 
   * @param block Block to validate
   * @return true if the block hash is correct and in target
   */
  public static boolean validate(Block block) {
    boolean targetHash = isTarget(block.hash);
    boolean correctHash = block.hash
        .equals(sha256(canonical(block.data, block.nonce)));
    return targetHash && correctHash;
  }

}

package pcd2017.exer2;

/**
 * The data for a block. To become part of the blockchain, a proof-of-work is needed to validate this data.
 */
public class BlockData {
  /**
   * The hash of the previous block in the chain
   */
  public final String previousHash;

  /**
   * The data payload of this block
   */
  public final String data;
  /**
   * This block's creation timestamp
   */
  public final long timestamp;

  public BlockData(String previousHash, String data, long timestamp) {
    super();
    this.previousHash = previousHash;
    this.data = data;
    this.timestamp = timestamp;
  }
}

## Quali differenze si ritiene distinguano i due algoritmi di mining?

Ho trovato differenze soprattutto sulla velocità di esecuzione e sulla leggibilità dei due algoritmi.
In particolare, threadMine sembra essere significativamente più veloce della controparte stream, almeno per hash corte (<=4).
Per quel che riguarda la leggibilità, streamMine è molto più compatto e leggibile di threadMine
 